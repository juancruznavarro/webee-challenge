# WebeeChallenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Install global dependencies
 
Run `npm install npm@latest -g`

Run `npm install -g @angular/cli`

## Install project dependencies

Run `npm install`

## Development server

Run `ng serve -o` for a dev server. 
