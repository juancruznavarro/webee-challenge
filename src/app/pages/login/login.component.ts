import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SessionService} from "../../services/session.service";
import {MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
  });
  loading: boolean;

  constructor(
    private userService: SessionService,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.form.valid) {
      this.loading = true;
      const data = this.form.value;
      this.userService.login(data.email, data.password)
        .subscribe((response: any) => {
          if (response.token) {
            this.userService.setUserLogged(data.email, response.token);
            this.snackBar.open('Welcome!!', '', { duration: 3000 });
            this.router.navigate(['/accounts']);
          }
          this.loading = false;
        }, () => {
          this.form.controls.password.reset();
          this.snackBar.open('Wrong username or password', '', { duration: 2000 });
          this.loading = false
        })
    }
  }

}
