import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-dialog-remove-user',
  templateUrl: './dialog-remove-user.component.html'
})
export class DialogRemoveUserComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

}
