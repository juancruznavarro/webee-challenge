import {Component, OnInit} from '@angular/core';
import {MatSnackBar, MatTableDataSource, MatDialog} from "@angular/material";
import {DialogAccountFormComponent} from "../form/dialog-account-form.component";
import {DialogRemoveUserComponent} from "./dialog-remove-user/dialog-remove-user.component";
import {AccountInterface} from "../../../models/account.interface";
import {AccountService} from "../../../services/account.service";

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit {
  displayedColumns: string[] = ['avatar', 'first_name', 'last_name', 'email', 'actions'];
  dataSource: MatTableDataSource<AccountInterface>;
  loading: boolean;

  constructor(
    private accountService: AccountService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.loading = true;
    this.accountService.getList()
      .subscribe(
        (response: any) => this.updateTable(response.data),
        () => this.error()
      );
  }

  create() {
    const dialogRef = this.dialog.open(DialogAccountFormComponent, {
      width: '500px',
      data: {
        action: 'CREATE'
      }
    });

    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.loading = true;
        if (response.ok && response.user) {
          const data = [...this.dataSource.data, response.user];
          this.updateTable(data);
          this.snackBar.open('User created successfully', '', {duration: 2000});
        } else {
          this.error();
        }
      }
    });
  }

  edit(user, index) {
    const dialogRef = this.dialog.open(DialogAccountFormComponent, {
      width: '500px',
      data: {
        action: 'UPDATE',
        user
      }
    });

    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.loading = true;
        if (response.ok && response.user) {
          let data = [...this.dataSource.data];
          data[index] = response.user;
          this.updateTable(data);
          this.snackBar.open('User updated successfully', '', {duration: 2000});
        } else {
          this.error();
        }
      }
    });

  }

  updateTable(data: AccountInterface[]) {
    this.dataSource = new MatTableDataSource<AccountInterface>(data);
    setTimeout(() => this.loading = false, 1000); // simulate delay
  }

  confirmRemove(user: AccountInterface, index) {
    const dialogRef = this.dialog.open(DialogRemoveUserComponent, {
      width: '550px',
      data: {
        user
      }
    });

    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.remove(user, index);
      }
    });
  }

  remove(data: AccountInterface, index) {
    this.loading = true;
    this.accountService.delete(data.id)
      .subscribe(() => {
        let data = [...this.dataSource.data];
        data.splice(index, 1);
        this.updateTable(data);
        this.loading = false;
        this.snackBar.open('User deleted successfully', '', {duration: 2000});
      }, () => this.error())
  }

  error() {
    this.loading = false;
    this.snackBar.open('An error has occurred, try again later', '', {duration: 2000});
  }

}
