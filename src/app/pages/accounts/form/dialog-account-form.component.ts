import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AccountInterface} from "../../../models/account.interface";
import {AccountService} from "../../../services/account.service";

@Component({
  selector: 'app-dialog-form',
  templateUrl: './dialog-account-form.component.html',
  styleUrls: ['./dialog-account-form.component.scss']
})
export class DialogAccountFormComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    first_name: new FormControl('', [Validators.required]),
    last_name: new FormControl('', [Validators.required]),
    avatar: new FormControl('', [
      Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')
    ])
  });
  loading: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogAccountFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private accountService: AccountService
  ) {
  }

  ngOnInit() {
    const user: AccountInterface = this.data.user;
    if (user) {
      this.form.patchValue(user);
    }
  }

  onSubmit() {
    this.loading = true;
    if (this.data.action === 'CREATE') {
      this.accountService.create(this.form.value)
        .subscribe(
          (user) => this.dialogRef.close({ok: true, user}),
          () => this.dialogRef.close({ok: false})
        )
    } else {
      const user: AccountInterface = this.data.user;
      this.accountService.update(user.id, this.form.value)
        .subscribe(
          (user) => this.dialogRef.close({ok: true, user}),
          () => this.dialogRef.close({ok: false})
        )
    }

  }

}
