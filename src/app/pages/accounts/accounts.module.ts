import {NgModule} from '@angular/core'
import {AccountsRoutingModule} from "./accounts-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AccountsListComponent} from "./list/accounts-list.component";
import {AppSharedModule} from "../../app-shared.module";
import {DialogAccountFormComponent} from "./form/dialog-account-form.component";
import {DialogRemoveUserComponent} from "./list/dialog-remove-user/dialog-remove-user.component";
import {AccountService} from "../../services/account.service";

@NgModule({
  imports: [
    AppSharedModule,
    AccountsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AccountsListComponent,
    DialogAccountFormComponent,
    DialogRemoveUserComponent
  ],
  entryComponents: [
    DialogAccountFormComponent,
    DialogRemoveUserComponent
  ],
  providers: [
    AccountService
  ]
})
export class AccountsModule {}
