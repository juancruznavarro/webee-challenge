import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../environments/environment";

@Injectable()
export class SessionService {

  constructor(
    private http: HttpClient
  ) {
  }

  login(email: string, password: string) {
    const data = {email, password};
    return this.http.post(environment.endpoint.apiUsers + '/login', data)
  }

  logout() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('email');
  }

  getUserLogged() {
    let response: any = {};
    if (this.isAuthenticated()) {
      response = {
        token: sessionStorage.getItem('token'),
        email: sessionStorage.getItem('email')
      }
    }
    return response
  }

  setUserLogged(email: string, token: string) {
    sessionStorage.setItem('email', email);
    sessionStorage.setItem('token', token);
    return this.getUserLogged();
  }

  isAuthenticated() {
    return !!sessionStorage.getItem('token') &&
      !!sessionStorage.getItem('email');
  }

}
