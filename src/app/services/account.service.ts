import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AccountService {

  constructor(private http: HttpClient) {
  }

  getList() {
    return this.http.get(environment.endpoint.apiUsers + '/users')
  }

  create(data) {
    return this.http.put(environment.endpoint.apiUsers + '/users', data)
  }

  update(id, data) {
    return this.http.put(`${environment.endpoint.apiUsers}/users/${id}`, data)
  }

  delete(id) {
    return this.http.delete(`${environment.endpoint.apiUsers}/users/${id}`)
  }
}
