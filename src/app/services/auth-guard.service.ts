import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {SessionService} from './session.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private user: SessionService,
    private router: Router
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const isAuth: boolean = this.user.isAuthenticated();
    if (!isAuth) {
      this.router.navigate(['/login']);
    }

    console.log(`isAuth ${isAuth}`);
    return isAuth;
  }
}
