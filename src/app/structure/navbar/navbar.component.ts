import {Component, OnInit} from '@angular/core';
import {SessionService} from "../../services/session.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  userLogged: any;

  constructor(
    private userService: SessionService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.userLogged = this.userService.getUserLogged();
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }

}
