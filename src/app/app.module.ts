import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {AppSharedModule} from "./app-shared.module";

// Services
import {AuthGuardService} from "./services/auth-guard.service";
import {SessionService} from "./services/session.service";

// Structure
import {LoginLayoutComponent} from './layouts/login-layout/login-layout.component';
import {MainLayoutComponent} from './layouts/main-layout/main-layout.component';
import {FooterComponent} from './structure/footer/footer.component';
import {NavbarComponent} from './structure/navbar/navbar.component';

// Pages
import {LoginComponent} from './pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginLayoutComponent,
    MainLayoutComponent,
    NavbarComponent,
    FooterComponent
  ],
  imports: [
    AppSharedModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [AuthGuardService, SessionService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
