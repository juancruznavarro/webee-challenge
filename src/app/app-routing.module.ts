import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./pages/login/login.component";
import {LoginLayoutComponent} from "./layouts/login-layout/login-layout.component";
import {MainLayoutComponent} from "./layouts/main-layout/main-layout.component";
import {AuthGuardService} from "./services/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent}
    ]
  },
  {
    path: 'accounts',
    component: MainLayoutComponent,
    loadChildren: () => import('./pages/accounts/accounts.module').then(mod => mod.AccountsModule),
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
