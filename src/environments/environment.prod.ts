export const environment = {
  production: true,
  endpoint: {
    apiUsers: 'https://reqres.in/api'
  }
};
